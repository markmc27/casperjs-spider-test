// Set the start URL
var startUrl = 'https://malmaison.equator-staging.com/';

// URL variables
var visitedUrls = [], pendingUrls = [];

// Create instances
var casper = require('casper').create({ /*verbose: true, logLevel: 'debug' */});
var utils = require('utils');
var helpers = {};
helpers['absoluteUri'] = absoluteUri;

// Spider from the given URL
function spider(url) {

	// Add the URL to the visited stack
	visitedUrls.push(url);

	// Open the URL
	casper.open(url).then(function() {

		// Set the status style based on server status code
		var status = this.status().currentHTTPStatus;
		switch(status) {
			case 200: var statusStyle = { fg: 'green', bold: true }; break;
			case 404: var statusStyle = { fg: 'red', bold: true }; break;
			 default: var statusStyle = { fg: 'magenta', bold: true }; break;
		}

		// Display the spidered URL and status
		this.echo(this.colorizer.format(status, statusStyle) + ' ' + url);

		// Find links present on this page
		var links = this.evaluate(function() {
			var links = [];
			Array.prototype.forEach.call(__utils__.findAll('a'), function(e) {
				links.push(e.getAttribute('href'));
			});
			return links;
		});

		// Add newly found URLs to the stack
		var baseUrl = this.getGlobal('location').origin;
		Array.prototype.forEach.call(links, function(link) {
			var newUrl = helpers.absoluteUri(baseUrl, link);
			if (pendingUrls.indexOf(newUrl) == -1 && visitedUrls.indexOf(newUrl) == -1) {
				//casper.echo(casper.colorizer.format('-> Pushed ' + newUrl + ' onto the stack', { fg: 'magenta' }));
				if(newUrl.indexOf(startUrl) > -1){ //only want to crawl links on our own site
					pendingUrls.push(newUrl);
				}
				
			}
		});

		// If there are URLs to be processed
		if (pendingUrls.length > 0) {
			var nextUrl = pendingUrls.shift();
			//this.echo(this.colorizer.format('<- Popped ' + nextUrl + ' from the stack', { fg: 'blue' }));
			spider(nextUrl);
		}

	});

}

// Start spidering
casper.start(startUrl, function() {
	spider(startUrl);
});

// Start the run
casper.run();


// Turn a (possibly) relative URI into a full RFC 3986-compliant URI
// With minor modifications, courtesy: https://gist.github.com/Yaffle/1088850
function absoluteUri(base, href) {

	// Parse a URI and return its constituent parts
	function parseUri(url) {
		var match = String(url).replace(/^\s+|\s+$/g, '').match(/^([^:\/?#]+:)?(\/\/(?:[^:@]*(?::[^:@]*)?@)?(([^:\/?#]*)(?::(\d*))?))?([^?#]*)(\?[^#]*)?(#[\s\S]*)?/);
		return (match ? { href: match[0] || '', protocol: match[1] || '', authority: match[2] || '', host: match[3] || '', hostname: match[4] || '',
		                  port: match[5] || '', pathname: match[6] || '', search: match[7] || '', hash: match[8] || '' } : null);
	}

	// Resolve dots in the path
	function resolvePathDots(input) {
		var output = [];
		input.replace(/^(\.\.?(\/|$))+/, '')
		     .replace(/\/(\.(\/|$))+/g, '/')
		     .replace(/\/\.\.$/, '/../')
		     .replace(/\/?[^\/]*/g, function (part) { part === '/..' ? output.pop() : output.push(part); });
		return output.join('').replace(/^\//, input.charAt(0) === '/' ? '/' : '');
	}

	// Parse base and href 
	href = parseUri(href || '');
	base = parseUri(base || '');

	// Build and return the URI 
	return !href || !base ? null : (href.protocol || base.protocol) +
	       (href.protocol || href.authority ? href.authority : base.authority) +
	       (resolvePathDots(href.protocol || href.authority || href.pathname.charAt(0) === '/' ? href.pathname : (href.pathname ? ((base.authority && !base.pathname ? '/' : '') + base.pathname.slice(0, base.pathname.lastIndexOf('/') + 1) + href.pathname) : base.pathname))) +
	       (href.protocol || href.authority || href.pathname ? href.search : (href.search || base.search)) + href.hash;

}

