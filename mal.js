var links = [];
var casper = require('casper').create({
    logLevel: 'log',
    verbose: true
});

function getLinks() {
    var links = document.querySelectorAll('.popupMenu a');
    return Array.prototype.map.call(links, function(e) {
        return e.getAttribute('href');
    });
}

casper.start('https://malmaison.equator-staging.com/', function() {
    this.evaluate(function(){document.querySelectorAll('.navToggler').click();});
    this.capture('nav open' + ".png");
	this.echo(this.status(true));
});

casper.then(function() {
    this.echo(this.status(true));
    // aggregate results for the 'casperjs' search
    links = this.evaluate(getLinks);
	this.echo(links);
});

casper.thenOpen('https://malmaison.equator-staging.com' + '/Account/Login/', function(response){
	this.echo('Open Link' , this.status(true));
	this.echo('Link url: ' + links[0] + ' \n');
	this.echo('Link title: ' + this.getTitle() + '\n');
	this.echo('\n');
	this.capture('images/' + 'a' + ".png");
	this.echo(this.status(true));
});

casper.then(function() {
    // aggregate results for the 'phantomjs' search
    links = links.concat(this.evaluate(getLinks));
    this.capture(this.getTitle() + ".png");
});

casper.run(function() {
    // echo results in some pretty fashion
    this.echo(links.length + ' links found:');
    this.echo(' - ' + links.join('\n - ')).exit();
});