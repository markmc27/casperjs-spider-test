var links = [];
var i = 0;
var casper = require('casper').create({
    logLevel: 'log',
    verbose: true
});

function getLinks() {
    var links = document.querySelectorAll('.popupMenu a');
    return Array.prototype.map.call(links, function(e) {
        return e.getAttribute('href');
    });
}

casper.start('https://malmaison.equator-staging.com/', function() {
    this.evaluate(function(){document.querySelectorAll('.navToggler').click();});
    this.capture('nav open' + ".png");
	this.echo(this.status(true));
});

casper.then(function() {
    this.echo(this.status(true));
    // aggregate results for the 'casperjs' search
    links = this.evaluate(getLinks);
	this.echo(links);
});


casper.then(function(){
	//this.echo(links);
	this.each(links, function(){
		//this.echo(links);
		//this.echo(i);
		//this.echo(links[i]);
		//this.echo('i = ', i);
		if(links[i].indexOf('malmaison') > -1){
			this.thenOpen((links[i]), function() {
				var strippedUrl = this.getCurrentUrl().replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '_');
				//console.log(strippedUrl);
				this.capture('images/' + strippedUrl + ".png");
				this.echo(this.getTitle() + ' link: ' + this.getCurrentUrl()); // display the title of page
			});
			
		}else{
			this.thenOpen('https://malmaison.equator-staging.com' + (links[i]), function() {
				var strippedUrl = this.getCurrentUrl().replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '_');
				//console.log(strippedUrl);
				this.capture('images/' + strippedUrl + ".png");
				this.echo(this.getTitle() + ' link: ' + this.getCurrentUrl()); // display the title of page
			});
		}
		i++;
	});

	//this.echo('Open Link' , this.status(true));
	//this.echo('Link url: ' + links[0] + ' \n');
	//this.echo('Link title: ' + this.getTitle() + '\n');
	//this.echo('\n');
	//this.capture('images/' + 'a' + ".png");
	//this.echo(this.status(true));
});

casper.run(function() {
    // echo results in some pretty fashion
    this.echo(links.length + ' links found:');
    //this.echo(' - ' + links.join('\n - ')).exit();
});