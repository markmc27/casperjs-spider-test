var links = [];
var i = 0;
var casper = require('casper').create({
    logLevel: 'log',
    verbose: true
});

function getLinks() {
    var links = document.querySelectorAll('.popupMenu a');
    return Array.prototype.map.call(links, function(e) {
        return e.getAttribute('href');
    });
}

casper.start('https://google.com/', function() {
    this.echo(this.getCurrentUrl());
	var pageForms = this.evaluate(function(){ return document.querySelectorAll('form'); });
	if(pageForms != null && pageForms.length > 0){
		this.echo(pageForms[0].name);
		//pageForms.forEach(function(form){
			//console.log(form);
			//console.log('');
		//});
	}
	else{
		this.echo('No forms on the page. \n');
	}
});


casper.run(function() {
    // echo results in some pretty fashion
    //this.echo(links.length + ' links found:');
    //this.echo(' - ' + links.join('\n - ')).exit();
	this.exit();
});