var links = [];
var i = 0;
var pageForms = [];
var casper = require('casper').create({
    logLevel: 'debug',
	verbose: true,
	pageSettings: {
		loadImages:  false,        // do not load images
    }
});

function getLinks() {
    var links = document.querySelectorAll('.popupMenu a');
    return Array.prototype.map.call(links, function(e) {
        return e.getAttribute('href');
    });
}

casper.start('https://malmaison.equator-staging.com/', function() {
    this.evaluate(function(){document.querySelectorAll('.navToggler').click();});
    this.capture('nav open' + ".png");
	this.echo(this.status(true));
});

casper.then(function() {
    this.echo(this.status(true));
    // aggregate results for the 'casperjs' search
    links = this.evaluate(getLinks);
	this.echo(links);
});


casper.then(function(){
	//this.echo(links);
	this.each(links, function analyzeLink(){
		//this.echo(links);
		//this.echo(i);
		//this.echo(links[i]);
		//this.echo('i = ', i);
		if(links[i].indexOf('malmaison') > -1){
			this.thenOpen((links[i]), function openLink() {
				this.echo(this.getCurrentUrl());
				pageForms = this.evaluate(function getForm(){ return document.querySelectorAll('form'); });
				if(pageForms != null && pageForms.length > 0){
					this.echo(pageForms[0].method + ': ' + pageForms[0].action);
					//pageForms.forEach(function(form){
						//this.echo(form);
						//this.echo('');
					//});
				}
				else{
					this.echo('No forms on the page. \n');
				}
				
				this.close();
			});
			
		}else{
			this.thenOpen('https://malmaison.equator-staging.com' + (links[i]), function openLocalLink() {
				this.echo(this.getCurrentUrl());
				pageForms = this.evaluate(function getForm2(){ return document.querySelector('form'); });
				if(pageForms != null && pageForms.length > 0){
					this.echo(pageForms[0].method + ': ' + pageForms[0].action);
				}
				else{
					this.echo('No forms on the page. \n');
				}
				
				this.close();
			});
		}
		i++;
	});

	//this.echo('Open Link' , this.status(true));
	//this.echo('Link url: ' + links[0] + ' \n');
	//this.echo('Link title: ' + this.getTitle() + '\n');
	//this.echo('\n');
	//this.capture('images/' + 'a' + ".png");
	//this.echo(this.status(true));
});

casper.run(function() {
    // echo results in some pretty fashion
    this.echo(links.length + ' links found:');
    //this.echo(' - ' + links.join('\n - ')).exit();
	this.exit();
});